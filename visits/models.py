from django.db import models
from django.utils.timezone import now

# Create your models here.


class User(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    login = models.CharField(max_length=50,default='')
    passwd = models.CharField(max_length=50, default='')

    def __str__(self):
        return self.name + ' ' + self.last_name


class Doctor(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

    def __str__(self):
        return self.name + ' ' + self.last_name


class Visit(models.Model):
    user = models.ForeignKey(User)
    doctor = models.ForeignKey(Doctor)
    startTime = models.DateTimeField()
    finTime = models.DateTimeField()
